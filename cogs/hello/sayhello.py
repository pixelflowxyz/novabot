from redbot.core import commands
import discord
class sayhello(commands.Cog):
    """My custom cog"""

    @commands.command()
    async def sayhello(self, ctx):
        """This does stuff!"""
        # Your code will go here
        await ctx.send("Hey everyone!")
    @commands.command(aliases=["8k"])
    async def eightthousand(self, ctx):
        if len(ctx.guild.members) > 8000:
            embed=discord.Embed(title="8000!", description="It's too late, we've reached 8000! It's a trap!", color=0xff0000)
            embed.set_author(name="Novabot", url="https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher",icon_url="http://novalauncher.com/nova_assets/img/icon_256x256.png")
            embed.set_thumbnail(url="http://novalauncher.com/nova_assets/img/icon_256x256.png")
        else:
            message = "There are {} members left until we reach 8000! <:tada:570314254360772649>".format(8000 - len(ctx.guild.members))
            embed=discord.Embed(title="8000!", description=message, color=0xff0000)
            embed.set_author(name="Novabot", url="https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher",icon_url="http://novalauncher.com/nova_assets/img/icon_256x256.png")
            embed.set_thumbnail(url="http://novalauncher.com/nova_assets/img/icon_256x256.png")
        await ctx.send(embed=embed)
